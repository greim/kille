// Auto Dark mode
window.onload = function() {
  
  // AutoDark Mode
  const preferedColorScheme = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";

  const setTheme = (theme) => {
      document.documentElement.setAttribute("class", theme);
  }

  setTheme(localStorage.getItem("theme") || preferedColorScheme);

  matchMedia("(prefers-color-scheme: dark)").addEventListener("change", applySystemTheme);

  function applySystemTheme() {
    const theme = matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
    document.documentElement.setAttribute("class", theme);
  }

  applySystemTheme();

  // window location href active class by url
  
  const nav_links = document.getElementById('gss-menu').getElementsByTagName('a');
  const nav_links_responsive = document.getElementById('gss-dropdown').getElementsByTagName('a');
  const nav_languages = document.getElementById('gss-menu-responsive-languages').getElementsByTagName('a');
  
  for (let i = 0; i < nav_links.length; ++i)
  if (nav_links[i].href === window.location.href)
  nav_links[i].className += ' active';
  
  for (let i = 0; i < nav_links_responsive.length; ++i)
  if (nav_links_responsive[i].href === window.location.href)
  nav_links_responsive[i].className += ' active';

  for (let i = 0; i < nav_languages.length; ++i)
  if (nav_languages[i].href === window.location.href)
  nav_languages[i].className += ' active';

};