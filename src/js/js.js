// Menus

const menuButton = document.getElementById('gss-menu-responsive');
const langsButton = document.getElementById('gss-menu-languages-button');

const menuitems = document.getElementById('gss-dropdown');
const langsItems = document.getElementById('gss-menu-responsive-languages');

menuButton.addEventListener('click', () => {  
  langsItems.classList.remove('are-languages');
  menuitems.classList.toggle('is-responsive');
});

langsButton.addEventListener('click', () => {
  menuitems.classList.remove('is-responsive');
  langsItems.classList.toggle('are-languages');
});