import lume from "lume/mod.ts";
import nunjucks from "lume/plugins/nunjucks.ts";
import sass from "lume/plugins/sass.ts";
import minifyHTML from "lume/plugins/minify_html.ts";
import basePath from "lume/plugins/base_path.ts";
import multilanguage from "lume/plugins/multilanguage.ts";
import inline from "lume/plugins/inline.ts";
import sitemap from "lume/plugins/sitemap.ts";
import favicon from "lume/plugins/favicon.ts";

const site = lume({
  src: "./src",
  dest: "./public",
  location: new URL("https://kille.es"),
});
site.use(multilanguage({
  languages: ["en", "gl", "es"],
  defaultLanguage: "en",
}));
site.use(nunjucks({
  includes: "includes",
}));
site.use(sass({
  includes: "sass",
}));
site.use(favicon({
  input: "img/favicon.svg",
}));
site.copy("img");
site.copy("js");
site.use(minifyHTML());
site.use(basePath());
site.ignore("sass");
site.ignore("includes");
site.use(inline());
site.use(sitemap());

export default site;



