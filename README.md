# Website

[![](https://img.shields.io/badge/www.kille.es-v0.3.0-%235C88DA.svg?longCache=false)](https://kille.es/)

# Develop

Play with the site: ```deno task serve```

# Framework

This website was made using the friendly static site generator: [Lume](https://lume.land/).